path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: ['./src/scripts/main.jsx', './src/assets/styles/styles.scss'],
  output: {
    filename: 'dist/assets/scripts/bundle.js'
  },
  module: {

    rules: [
      {
        test: /\.jsx$/,
        loaders: ['babel-loader'],
        exclude: /(node_modules)/
      },
      {
        test: /\.js$/,
        loaders: ['babel-loader'],
        exclude: /(node_modules)/
      },
      { // regular css files
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          use: 'css-loader?importLoaders=1',
        }),
      },
      { // sass / scss loader for webpack
        test: /\.(sass|scss)$/,
        loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new ExtractTextPlugin({ // define where to save the file
      filename: 'dist/assets/styles/[name].bundle.css',
      allChunks: true,
    }),
    new CopyWebpackPlugin([
      { from: 'src/assets/images', to: 'dist/assets/images'}
    ]),
    new webpack.optimize.UglifyJsPlugin({
      mangle: false,
      sourcemap: true,
      compress: {
        warnings: false
      },
      output: {
        comments: false
      },
    }),
    new webpack.optimize.AggressiveMergingPlugin()
  ],
};