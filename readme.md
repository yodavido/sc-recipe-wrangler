# Sam's Club Recipe Wrangler.

Recipe Wrangler is a central hub to administer current recipes that are being used in all sams club executions as well as create and add new Suppliers, Programs/ConvergeIDs, and Recipes.

It current resides at: [Recipe Wrangler](http://scmsb/Global/Recipe-Wrangler/)
## Getting Started


### Installing

To get started all that's needed is to clone this repo locally.

```
git@gitlab.com:trm-sc/recipe-wrangler-final.git
```

Change directories into recipe-wrangler-final.

Then run:

```
npm install
```

Once installed to demo locally:

* Locate the stores directory 
* Open the RecipeStore.jsx, SupplierStore.jsx, & Programs.jsx files
* Change the endpoints in the axios calls from the service urls to point toward the fixture data in the fixtures folder.


## Running Locally

To run a locally hosted dev server run:

```
npm start
```

This will auto start a local dev server and automatically pop open a browser tab.
Any changes to any .js, .jsx, .css, and .scss will automatically hot reload the app in browser.

## Building Changes

To build any changes made:

* First be sure the axios endpoints are changed back to the correct service urls

Then Run:

```
npm run build
```

This will compile all .jsx and .js to a bundled, and uglified bundle.js.

This will also compile all .scss and .css to a main.bundle.css file.

Both the bundle.js and main.bundle.css will be compiled into the dist folder along with any images in the /src/assets/images will be copied to the /dist/assets/images folder.

## Moving to SCMSB

Once its been build the contents of the dist folder can be copied anywhere on the Sams Club dev server.

After it's been copied the only other step is to change the file name of:

```
index.html
```

to

```
default.aspx
```

At that point it should automatically be able to connect to the database.