export default class SupplierModel {
  SupplierID;
  SupplierName;
  ConvergeIDs;

  constructor (supplier) {
    this.SupplierID = supplier.SupplierID;
    this.SupplierName = supplier.SupplierName;
    this.ConvergeIDs = supplier.ConvergeIDs;
  }
}