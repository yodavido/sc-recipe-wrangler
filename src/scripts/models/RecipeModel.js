import { observable } from 'mobx';

export default class RecipeModel {
  store;
  RecipeID;
  @observable RecipeName;
  @observable RecipeImage;
  @observable LogoImage;
  @observable PrepTime;
  @observable CookTime;
  @observable Course;
  @observable Category;
  @observable Servings;
  @observable RomanceCopy;
  @observable Ingredients;
  @observable CookingInstructions;
  @observable RecipeNotes;
  @observable Tracking;
  @observable MediaType;
  @observable Featured;
  @observable Approved;
  @observable ModifiedDate;
  AvgRating;
  NumberOfRatings;
  @observable OrderIndex;
  @observable Brand;
  @observable LiveDate;
  @observable ConvergeIDs;

  constructor (recipe) {
    this.store = recipe.store;
    this.RecipeID = recipe.RecipeID;
    this.RecipeName = recipe.RecipeName;
    this.RecipeImage = recipe.RecipeImage;
    this.LogoImage = recipe.LogoImage;
    this.PrepTime = recipe.PrepTime;
    this.CookTime = recipe.CookTime;
    this.Course = recipe.Course !== "" ? recipe.Course.split(',') : [];
    this.Category = recipe.Category !== "" ? recipe.Category.split(',') : [];
    this.Servings = recipe.Servings;
    this.RomanceCopy = recipe.RomanceCopy;
    this.Ingredients = recipe.Ingredients !== "" ? recipe.Ingredients.split('||') : [];
    this.CookingInstructions = recipe.CookingInstructions !== "" ? recipe.CookingInstructions.split('||') : [];
    this.RecipeNotes = recipe.RecipeNotes;
    this.Tracking = recipe.Tracking;
    this.MediaType = recipe.MediaType;
    this.Featured = recipe.Featured;
    this.Approved = recipe.Approved;
    this.ModifiedDate = recipe.ModifiedDate;
    this.AvgRating = recipe.AvgRating;
    this.NumberOfRatings = recipe.NumberOfRatings;
    this.OrderIndex = recipe.OrderIndex;
    this.Brand = recipe.Brand;
    this.LiveDate = recipe.LiveDate;
    this.ConvergeIDs = recipe.ConvergeIDs;
  }

  destroy () {
    this.store.deleteRecipe(this)
  }

  setProp (key, val) {
    console.log(key, val);
    this[key] = val;
  }

  updateRecipe () {
    this.store.updateRecipe(this);
  }

  addIngredient (val) {
    let ings = this.Ingredients
    ings.push(val);

    this.setProp('Ingredients', ings);
  }

  updateIngredient (index, value) {
    let ings = this.Ingredients;
    ings[index] = value;

    this.setProp('Ingredients', ings);
  }

  deleteIngredient (i) {
    let ings = this.Ingredients
    ings.splice(i, 1);
    this.setProp('Ingredients', ings)
  }

  addInstruction (val) {
    let insts = this.CookingInstructions
    insts.push(val);

    this.setProp('CookingInstructions', insts);
  }

  updateInstruction (index, value) {
    let insts = this.CookingInstructions;
    insts[index] = value;

    this.setProp('CookingInstructions', insts);
  }

  deleteInstruction (i) {
    let insts = this.CookingInstructions
    insts.splice(i, 1);
    this.setProp('CookingInstructions', insts)
  }

  addCourse (c) {
    let courses = this.Course;
    if(courses.indexOf(c) <= -1 && c.length > 0)
      courses.push(c);

    this.setProp('Course', courses)
  }
  removeCourse (c) {
    let courses = this.Course
    if(courses.indexOf(c) > -1)
      courses.splice(courses.indexOf(c), 1)

    this.setProp('Course', courses)
  }

  addCategory (c) {
    let cats = this.Category;
    if(cats.indexOf(c) <= -1 && c.length > 0)
      cats.push(c);
    this.setProp('Category', cats)
  }
  removeCategory (c) {
    let cats = this.Category
    if(cats.indexOf(c) > -1)
      cats.splice(cats.indexOf(c), 1)

    this.setProp('Category', cats)
  }
}