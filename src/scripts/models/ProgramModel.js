export default class ProgramModel {
  ConvergeID;
  RecipeCount;

  constructor (program) {
    this.ConvergeID = program.ConvergeID;
    this.RecipeCount = program.RecipeCount;
  }
}