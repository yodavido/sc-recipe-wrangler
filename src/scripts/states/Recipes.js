import { observable } from 'mobx';

export class REditor {
  @observable recipe = {};
  @observable editorType = 'new';
  @observable addingCat = false;
  @observable addingCourse = false;
  @observable addingIng = false;
  @observable addingIns = false;
  @observable catVal = '';
  @observable courseVal = '';
  @observable ingVal = '';
  @observable insVal = '';
  @observable listItemEditor = {
    open: false,
    itemType: '',
    itemIndex: 0,
    itemValue: '',
  };
}

export class Recipe {
  @observable deleteOpen = false;
  @observable deleting = false;
  @observable viewMedia = false;
  @observable editing = false;
}

export class RGrid {
  @observable viewMode = 'grid';
  @observable deleteOpen = false;
  @observable viewMedia = false;
}