import React from 'react';
import { render } from 'react-dom';

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

import { routes } from './routes.jsx';

render(
  routes,
  document.getElementById('app')
);