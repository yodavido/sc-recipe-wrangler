import React from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import { Provider } from 'mobx-react';

/**
 * Data Stores
 * @type {{}}
 */
import RecipeStore from './stores/RecipeStore';
import SupplierStore from './stores/SupplierStore';
import ProgramStore from './stores/ProgramStore';

const recipeStore = new RecipeStore();
const supplierStore = new SupplierStore();
const programStore = new ProgramStore();


const stores = { recipeStore, supplierStore, programStore };

/**
 * App Views
 * @type {{}}
 */
import App from './views/app.jsx';
import Home from './views/home.jsx';
import Recipes from './views/recipes.jsx';
import Recipe from './views/recipe.jsx';
import RecipeNew from './views/recipeNew.jsx';
import Suppliers from './views/suppliers.jsx';
import Supplier from './views/supplier.jsx';
import Programs from './views/programs.jsx';
import Program from './views/program.jsx';

/**
 * App routes
 * @type {XML}
 */
export const routes = (
  <Provider {...stores}>
    <Router onUpdate={() => window.scrollTo(0, 0)} history={ hashHistory }>
      <Route path="/" component={ App }>
        <IndexRoute component={ Home } />
        <Route path="/recipes" component={ Recipes } />
        <Route path="/recipes/:_rid" component={ Recipe } />
        <Route path="/recipes-new" component={ RecipeNew } />
        <Route path="/suppliers" component={ Suppliers } />
        <Route path="/suppliers/:_sid" component={ Supplier } />
        <Route path="/programs" component={ Programs } />
        <Route path="/programs/:_pid" component={ Program }/>
      </Route>
    </Router>
  </Provider>
)