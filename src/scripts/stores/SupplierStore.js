import { observable, action } from 'mobx';
import SupplierModel from '../models/SupplierModel';
import axios from 'axios';

export default class SupplierStore {
  @observable suppliers =[];
              originalSuppliers = [];
  @observable supplier = {};
  @observable suppliersLoaded = false;
  @observable supplierLoaded = false;
  @observable supplierSaved = false;

  /**
   * Retrieves all suppliers from the database.
   */
  @action getAllSuppliers () {
    this.suppliersLoaded = false;

    axios.get('/services/svcRecipeHubAdmin.asmx/GetSupplier_All_JSON ')
      .then((res)=>{
        this.suppliers = res.data.map((s)=>{
          return new SupplierModel(s);
        });
        this.originalSuppliers = this.suppliers;

        this.suppliersLoaded = true;
      });
  }

  /**
   * Retrieves specific supplier based on SupplierID and returns an axios promise.
   * @param sid
   * @returns {AxiosPromise}
   */
  @action getSupplierById (sid) {
    this.supplierLoaded = false;
    return axios.get('/services/svcRecipeHubAdmin.asmx/GetSupplierByID_JSON?SupplierID=' + sid);
  }

  /**
   * Creates new supplier and returns an axios promise to be used in recipeNew view
   * @param val
   * @returns {AxiosPromise}
   */
  @action addNewSupplier (val) {
    return axios({
      method: 'post',
      url: '/services/svcRecipeHubAdmin.asmx/InsertSupplier_JSON',
      data:{
        SupplierName: val
      }
    });
  }
}