import { observable, action } from 'mobx';
import ProgramModel from '../models/ProgramModel';
import axios from 'axios';

export default class SupplierStore {
  @observable programs =[];
  originalPrograms = [];
  @observable program = {};
  @observable programsLoaded = false;
  @observable programLoaded = false;

  /**
   * Retrieves all Programs from the database
   */
  @action getAllPrograms () {
    this.programsLoaded = false;

    axios.get('/services/svcRecipeHubAdmin.asmx/GetConverge_All_JSON ')
      .then((res)=>{
        this.programs = res.data.map((p)=>{
          return new ProgramModel(p);
        });
        this.originalPrograms = this.programs;

        this.programsLoaded = true;
      });
  }

  /**
   * Searches programs
   * @param val
   */
  @action searchProgram (val) {
    this.programs = this.originalPrograms.filter(
      (c) => {
        return c.ConvergeID.toLowerCase().search(val.toLowerCase()) > -1
      }
    )
  }

  /**
   * Adds new program and returns an AxiosPromise to be used in the recipeNew view.
   * @param program
   * @returns {AxiosPromise}
   */
  @action addNewProgram (program) {
    return axios({
      method: 'post',
      url: '/services/svcRecipeHubAdmin.asmx/InsertConvergeForSupplier_JSON',
      data: {
        SupplierID: program.supplierId,
        ConvergeID: program.convergeId
      }
    })
  }
}