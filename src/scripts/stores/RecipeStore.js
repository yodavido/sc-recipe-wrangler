import { observable, action } from 'mobx';
import RecipeModel from '../models/RecipeModel';
import axios from 'axios';

class RecipeStore {

  @observable recipes = [];
              originalRecipes = [];
  @observable recipe = {};
              originalRecipe = {};
  @observable recipesLoaded = false;
  @observable recipeLoaded = false;

  /**
   * Requests all recipes in the database.
   */
  @action getAllRecipes () {
    this.recipesLoaded = false;
    this.recipes = [];
    this.originalRecipes = [];
    axios.get('/services/svcRecipeHubAdmin.asmx/GetRecipe_All_JSON')
      .then((res)=>{
        this.recipes = res.data.map((r)=>{
          let tmp = r;
          tmp.store = this;
          return new RecipeModel(tmp);
        });
        this.originalRecipes = this.recipes;

        this.recipesLoaded = true;
      });
  }

  /**
   * Request all recipes under a single converge
   * @param cid
   */
  @action getRecipesByConverge (cid) {
    this.recipesLoaded = false;
    this.recipes = [];
    this.originalRecipes = [];
    axios.get('/services/svcRecipeHubAdmin.asmx/GetRecipe_ByConverge_JSON?ConvergeID=' + cid)
      .then((res)=>{
        this.recipes = res.data.map((r)=>{
          let tmp = r;
          tmp.store = this;
          return new RecipeModel(tmp);
        });
        this.originalRecipes = this.recipes;

        this.recipesLoaded = true;
      });
  }

  /**
   * Requests a specific recipe by id
   * @param rid
   */
  @action getRecipeById (rid) {
    this.recipeLoaded = false;

    axios.get(`/services/svcRecipeHubAdmin.asmx/GetRecipe_ByRecipeID_JSON?RecipeID=${rid}`)
      .then((res)=>{
        let tmp = res.data[0];
        tmp.store = this;
        this.recipe = new RecipeModel(tmp);
        this.originalRecipe = new RecipeModel(tmp);
        this.recipe.store = this;
        this.recipesLoaded = true;
      });
  }

  /**
   * Updates a recipe after it's been edited
   * @param recipe
   */
  @action updateRecipe (recipe) {
    let urlbase = '/services/svcRecipeHubAdmin.asmx/UpdateRecipe_JSON';
    let { RecipeID } = recipe;
    let params = {
      RecipeID: recipe.RecipeID,
      RecipeName: recipe.RecipeName.length > 0 ? recipe.RecipeName : this.originalRecipe.RecipeName,
      RecipeImage: recipe.RecipeImage.length > 0 ? recipe.RecipeImage : this.originalRecipe.RecipeImage,
      LogoImage: recipe.LogoImage.length > 0 ? recipe.LogoImage : this.originalRecipe.LogoImage,
      PrepTime: recipe.PrepTime.length > 0 ? recipe.PrepTime : this.originalRecipe.PrepTime,
      CookTime: recipe.CookTime.length > 0 ? recipe.CookTime : this.originalRecipe.CookTime,
      Course: recipe.Course.length > 0 ? recipe.Course.join(',') : '',
      Category: recipe.Category.length > 0 ? recipe.Category.join(',') : '',
      Servings: recipe.Servings.length > 0 ? recipe.Servings : this.originalRecipe.Servings,
      RomanceCopy: recipe.RomanceCopy.length > 0 ? recipe.RomanceCopy : this.originalRecipe.RomanceCopy,
      Ingredients: recipe.Ingredients.join("||"),
      CookingInstructions: recipe.CookingInstructions.join("||"),
      RecipeNotes: recipe.RecipeNotes.length > 0 ? recipe.RecipeNotes : this.originalRecipe.RecipeNotes,
      Tracking: recipe.Tracking.length > 0 ? recipe.Tracking : this.originalRecipe.Tracking,
      MediaType: recipe.MediaType.length > 0 ? recipe.MediaType : this.originalRecipe.MediaType,
      Featured: recipe.Featured,
      Approved: recipe.Approved,
      OrderIndex: recipe.OrderIndex,
      Brand: recipe.Brand,
      LiveDate: recipe.LiveDate
    }

    axios ({
      method: 'post',
      url: urlbase,
      data: params
    })
    .then(()=>window.location = '#/recipes/' + RecipeID)
    .catch((error)=>console.log(error));
  }

  /**
   * Searches the list of recipes
   * @param query
   */
  @action searchRecipes (query) {
    this.recipes = this.originalRecipes.filter((r) => {
      return r.RecipeName.toLowerCase().search(query.toLowerCase()) > -1;
    });
  }

  /**
   * Initializes a new recipe to be inserted into the database.
   */
  @action initNewRecipe () {
    this.recipeLoaded = true;
    let tmp = {
      store: this,
      RecipeID: 0,
      RecipeName: 'Enter a name',
      RecipeImage: 'Enter an image name.',
      LogoImage: 'Enter a logo name',
      PrepTime: '0',
      CookTime: '0',
      Course: '',
      Category: '',
      Servings: '0',
      RomanceCopy: 'Enter some romantic copy',
      Ingredients: '',
      CookingInstructions: '',
      RecipeNotes: 'Note some stuff',
      Tracking: 'Add a tracking name.',
      MediaType: 'Is this a video or image recipe',
      Featured: false,
      Approved: true,
      ModifiedDate: new Date(),
      AvgRating: 0,
      NumberOfRatings: 0,
      OrderIndex: 0,
      Brand: '',
      LiveDate: '',
      ConvergeIDs: []
    }
    this.recipe = new RecipeModel(tmp);
  }

  /**
   * Adds newly initialized recipe to database
   * @param recipe
   */
  @action addNewRecipe (recipe) {
    let urlbase = '/services/svcRecipeHubAdmin.asmx/InsertRecipe_JSON';

    let params = {
      RecipeID: recipe.RecipeID,
      ConvergeID: recipe.ConvergeIDs[0],
      RecipeName: recipe.RecipeName.length > 0 ? recipe.RecipeName : this.originalRecipe.RecipeName,
      RecipeImage: recipe.RecipeImage.length > 0 ? recipe.RecipeImage : this.originalRecipe.RecipeImage,
      LogoImage: recipe.LogoImage.length > 0 ? recipe.LogoImage : this.originalRecipe.LogoImage,
      PrepTime: recipe.PrepTime.length > 0 ? recipe.PrepTime : this.originalRecipe.PrepTime,
      CookTime: recipe.CookTime.length > 0 ? recipe.CookTime : this.originalRecipe.CookTime,
      Course: recipe.Course.length > 0 ? recipe.Course.join(',') : '',
      Category: recipe.Category.length > 0 ? recipe.Category.join(',') : '',
      Servings: recipe.Servings.length > 0 ? recipe.Servings : this.originalRecipe.Servings,
      RomanceCopy: recipe.RomanceCopy.length > 0 ? recipe.RomanceCopy : ' ',
      Ingredients: recipe.Ingredients.join("||"),
      CookingInstructions: recipe.CookingInstructions.join("||"),
      RecipeNotes: recipe.RecipeNotes.length > 0 && recipe.RecipeNotes.search('Note some stuff') <= -1 ? recipe.RecipeNotes : ' ',
      Tracking: recipe.Tracking.length > 0 ? recipe.Tracking : this.originalRecipe.Tracking,
      MediaType: recipe.MediaType.length > 0 ? recipe.MediaType : this.originalRecipe.MediaType,
      Featured: recipe.Featured,
      Approved: recipe.Approved,
      OrderIndex: recipe.OrderIndex,
      Brand: recipe.Brand,
      LiveDate: recipe.LiveDate
    }

    axios({
      method: 'post',
      url: urlbase,
      data: params
    })
      .then((res)=>{
        let recipeString = String(res.data).split(',')[0];
        let rid = recipeString.split(':')[1];

        window.location = "#/recipes/" + rid;
      }).catch(function (error) {
      console.log(error);
    });
  }

  /**
   * Deletes a specific recipe by id and returns axiosPromise to ensure redirect to previous route after deletion
   * @param rid
   */
  @action deleteRecipe (rid) {
    return axios.get(`/services/svcRecipeHubAdmin.asmx/DeleteRecipe_JSON?RecipeID=${rid}`)

  }
}

export default RecipeStore;