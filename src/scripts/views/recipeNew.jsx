import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { observable } from 'mobx';

import RecipeNewStep3 from '../components/newRecipes/RecipeNewStep3.jsx';

@inject('recipeStore', 'supplierStore', 'programStore') @observer
export default class Recipe extends Component {
  /**
   * Todo: Move all observables to STATE class and import.
   * @type {{}}
   */
  @observable selectedSupplier = {};
  @observable selectedConvergeID = '';
  @observable newSupplier = false;
  @observable supplierVal = '';
  @observable convergeVal = '';
  @observable currentStep = 1;
  @observable addingNewSupplier = false;
  @observable addingNewConverge = false;
  @observable convergeSaved = false;

  componentWillMount () {
    this.props.supplierStore.getAllSuppliers();
  }

  /**
   * Event handler for when the user selects a supplier
   * @param e
   */
  onSupplierSelect (e) {
    const { supplierStore } = this.props;
    const { suppliers } = supplierStore;
    this.selectedSupplier = suppliers[e.target.value];
    this.currentStep = e.target.value == "Select a supplier." ? 1 : 2;
  }

  /**
   * Event Handler for when a new supplier is created and saved to database.
   */
  onSupplierSave () {
    const { supplierStore } = this.props;
    supplierStore.addNewSupplier(this.supplierVal)
      .then((res)=>{
        this.selectedSupplier = JSON.parse(res.data.split('{"d":null}')[0])[0];
        supplierStore.suppliersLoaded = false;
        supplierStore.getAllSuppliers();
        this.addingNewSupplier = false;
        this.currentStep = 2;
      }).catch((error)=>console.log(error));
  }

  /**
   * Event handler for when a new converge/program is created and saved to database.
   */
  onConvergeSave () {
    const { programStore, supplierStore } = this.props;
    programStore.addNewProgram({supplierId: this.selectedSupplier.SupplierID, convergeId: this.convergeVal})
      .then(()=>{
        supplierStore.suppliersLoaded = false;
        supplierStore.getAllSuppliers();
        this.addingNewConverge = false;
        this.selectedConvergeID = this.convergeVal;
        this.currentStep = 3;
      }).catch((error)=>console.log(error));
  }

  /**
   * Event handler for when an existing converge is selected after supplier is.
   * @param e
   */
  onConvergeSelect (e){
    this.selectedConvergeID = e.target.value;
    this.currentStep = isNaN(parseInt(e.target.value)) ? 2 : 3;
  }

  render () {
    const { supplierStore } = this.props;
    const { suppliers } = supplierStore;

    return (
      <span className={`recipe-new ${this.convergeSaved ? "show" : ''}`}>
        <div className="block saved">
          <span className="tag is-success">
            ConvergeID Saved
            <button className="delete is-small" onClick={()=>this.convergeSaved=false}></button>
          </span>
        </div>

        <div className="hero">
          <div className="hero-body" style={{paddingBottom: 0}}>
            <div className="container has-text-centered">
              <div className="columns">
                <div className="column is-half is-offset-one-quarter">
                  <nav className="pagination is-centered">
                    <ul className="pagination-list">
                      <li><a className={`pagination-link ${this.currentStep == 1 ? 'is-current' : ''} is-success`} disabled>1</a></li>
                      <li><a className={`pagination-link ${this.currentStep == 2 ? 'is-current' : ''} is-success`} disabled>2</a></li>
                      <li><a className={`pagination-link ${this.currentStep == 3 ? 'is-current' : ''} is-success`} disabled>3</a></li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.currentStep > 0 && this.currentStep < 3 ?
          <div className="recipe">
            <div className="hero">
              <div className="hero-body" style={{paddingBottom: 0}}>
                <div className="container has-text-centered">
                  <div className="columns">
                    <div className="column is-half is-offset-one-quarter">
                      Select a supplier:
                    </div>
                  </div>
                  <div className="columns">
                    <div className="column is-half is-offset-one-quarter">
                      <div className="field has-addons has-addons-centered">
                        <p className="control">
                          <span className="select is-large" style={{display: 'inline-block', margin: '0 auto'}}>
                            <select onChange={this.onSupplierSelect.bind(this)}>
                              <option>Select a supplier.</option>
                              { suppliers ?
                                suppliers.map((c, i)=>{
                                  return <option key={c.SupplierID} value={i} selected={c.SupplierName == this.supplierVal}>{c.SupplierName}</option>;
                                }): ""}
                            </select>
                          </span>
                        </p>
                      </div>
                    </div>
                  </div>
                  { this.addingNewSupplier ?
                    <div className="columns">
                      <div className="column is-half is-offset-one-quarter">
                        <div className="field is-grouped">
                          <p className="control is-expanded">
                            <input className="input" type="text" placeholder="Supplier Name" onChange={(e)=>this.supplierVal=e.target.value} />
                          </p>
                          <p className="control">
                            <a onClick={this.onSupplierSave.bind(this)}><span className="icon is-small"><i className="fa fa-check"></i></span></a>
                            <a onClick={()=>this.addingNewSupplier = false}><span className="icon is-small"><i className="fa fa-close"></i></span></a>
                          </p>
                        </div>
                      </div>
                    </div> :
                    <span>
                      <div className="columns">
                        <div className="column is-half is-offset-one-quarter">
                          or add a new supplier:
                        </div>
                      </div>
                      <div className="columns is-fullwidth">
                        <div className="column is-2 is-centered">
                          <a onClick={()=>this.addingNewSupplier = true} className="button is-success is-outlined is-fullwidth">+</a>
                        </div>
                      </div>
                    </span> }
                </div>
              </div>
            </div>
          </div>: ''}
        {this.currentStep > 1 && this.currentStep < 3 ?
          <div className="recipe">
            <div className="hero">
              <div className="hero-body">
                <div className="container has-text-centered">
                  { this.selectedSupplier.ConvergeIDs && Object.keys(this.selectedSupplier.ConvergeIDs).length > 0 ?
                    <div className="columns">
                      <div className="column is-half is-offset-one-quarter">
                        Select a convergeID:
                      </div>
                    </div> :
                    '' }
                  <div className="columns">
                    <div className="column is-half is-offset-one-quarter">
                      { this.selectedSupplier.ConvergeIDs && Object.keys(this.selectedSupplier.ConvergeIDs).length > 0 ?
                        <div className="field has-addons has-addons-centered">
                          <p className="control">
                          <span className="select is-large" style={{display: 'inline-block', margin: '0 auto'}}>
                            <select onChange={this.onConvergeSelect.bind(this)}>
                              <option>Select a convergeID.</option>
                              {Object.entries(this.selectedSupplier.ConvergeIDs).map((c, i)=>{
                                  return <option key={c[0]} value={c[0]}>{c[0]}</option>;
                              })}
                            </select>
                          </span>
                          </p>
                        </div> :
                        ''}
                    </div>
                  </div>
                  { this.addingNewConverge ?
                    <span>
                      <div className="columns">
                        <div className="column is-half is-offset-one-quarter">
                          { this.selectedSupplier.ConvergeIDs && Object.keys(this.selectedSupplier.ConvergeIDs).length > 0 ? 'or' : ''} add a new convergeID:
                        </div>
                      </div>
                      <div className="columns">
                        <div className="column is-half is-offset-one-quarter">
                          <div className="field is-grouped">
                            <p className="control is-expanded">
                              <input className="input" type="text" placeholder="ConvergeID" onChange={(e)=>this.convergeVal=e.target.value} />
                            </p>
                            <p className="control">
                              <a onClick={this.onConvergeSave.bind(this)}><span className="icon is-small"><i className="fa fa-check"></i></span></a>
                              <a onClick={()=>this.addingNewConverge = false}><span className="icon is-small"><i className="fa fa-close"></i></span></a>
                            </p>
                          </div>
                        </div>
                      </div>
                    </span>:
                    <span>
                      <div className="columns">
                        <div className="column is-half is-offset-one-quarter">
                          { this.selectedSupplier.ConvergeIDs && Object.keys(this.selectedSupplier.ConvergeIDs).length > 0 ? 'or' : ''} add a new convergeID:
                        </div>
                      </div>
                      <div className="columns is-fullwidth">
                        <div className="column is-2 is-centered">
                          <a onClick={()=>this.addingNewConverge = true} className="button is-success is-outlined is-fullwidth">+</a>
                        </div>
                      </div>
                    </span> }
                </div>
              </div>
            </div>
          </div>: ''}
        {this.currentStep == 3 ?
          <RecipeNewStep3 recipeStore={this.props.recipeStore} converge={this.selectedConvergeID} /> :
          ''}
      </span>

    )
  }
}