import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import RecipesGrid   from '../components/recipes/RecipesGrid.jsx';
import RecipesList   from '../components/recipes/RecipesList.jsx';

import { RGrid } from '../states/Recipes';


let STATE = new RGrid();

@inject('recipeStore') @observer
export default class Recipes extends Component {

  componentWillMount () {
    this.props.recipeStore.getAllRecipes();
  }

  /**
   * Event handler for when the user clicks the view switch between "grid" and "list" view.
   * @param e
   */
  switchView (e) {
    e.preventDefault();
    STATE.viewMode = STATE.viewMode == "grid" ? "list" : "grid";
  }

  /**
   * Event handler for recipe searches
   * @param e
   */
  onSearchChange (e) {
    let { value } = e.target;
    this.props.recipeStore.searchRecipes(value);
  }

  /**
   * Creates elements for css Loading animation
   * @returns {XML}
   * @constructor
   */
  Loader = () => {
    return(
      <div className="spinner">
        <div className="cube1"></div>
        <div className="cube2"></div>
      </div>
    )
  }

  render () {
    const { recipeStore } = this.props;
    const { recipes } = recipeStore;

    return (
      <div>
        <div className="hero">
          <div className="hero-body">
            <div className="container has-text-centered">
              <h1 className="title">{recipes ? recipes.length : '' } Recipes</h1>
              <h2 className="subtitle">Search and update recipes</h2>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column is-half is-offset-one-quarter search-box">
            <div className="field">
              <p className="control has-icons-left has-icons-right">
                <input className="input" type="text" placeholder="Search" onChange={(e)=>this.onSearchChange(e)}/>
                <span className="icon is-left">
                  <i className="fa fa-search"></i>
                </span>
              </p>
            </div>
          </div>
          <div className="column is-one-quarter">
            <a href="" className="viewMode" onClick={this.switchView}>
              { STATE.viewMode == "grid" ?
                <i className="fa fa-th-list" aria-hidden="true"></i> :
                <i className="fa fa-th-large" aria-hidden="true"></i> }
            </a>
          </div>
        </div>
        <div className="columns">
          <div className="column is-12">
            { recipes.length > 0 ?
              (STATE.viewMode == "grid" ?
              <RecipesGrid recipes={recipes}/> :
              <RecipesList recipes={recipes}/>) :
              this.Loader() }
          </div>
        </div>
      </div>
    )
  }

}