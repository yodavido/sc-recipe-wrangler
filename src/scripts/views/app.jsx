import React, { Component } from 'react';
import { Link } from 'react-router';

import '../../assets/styles/styles.scss';

export default class App extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const { pathname } = this.props.router.getCurrentLocation();
    return (
      <div>
        <header className="header">
          <img src="assets/images/samsLogo.png" alt="" className="logo" />
        </header>
        <div className="is-fullwidth">
          <div className="tabs is-centered has-shadow">
            <ul className={`nav-right nav-menu has-shadow ${this.toggleMenu ? 'is-active' : ''}`}>
              <li className={pathname == "/" ? 'is-active' : ''}>
                <Link to="/"><i className="fa fa-home" aria-hidden="true"></i></Link>
              </li>
              <li className={pathname.search("recipes") > -1 ? 'is-active' : ''}>
                <Link to="/recipes">
                  <span className="icon is-small"><i className="fa fa-cutlery"></i></span> RECIPES
                </Link>
              </li>
              <li className={pathname.search("suppliers") > -1 ? 'is-active' : ''}>
                <Link to="/suppliers">
                  <span className="icon is-small"><i className="fa fa-shopping-basket"></i></span> SUPPLIERS
                </Link>
              </li>
              <li className={pathname.search("programs") > -1 ? 'is-active' : ''}>
                <Link to="/programs">
                  <span className="icon is-small"><i className="fa fa-bar-chart"></i></span> PROGRAMS
                </Link>
              </li>
              <li className={pathname == "/recipes-new" ? 'is-active' : ''}><Link to="/recipes-new"><i className="fa fa-plus"></i></Link></li>
            </ul>
          </div>
        </div>

        <div className="mainRegion">
          <div className="container">
            { this.props.children }
          </div>
        </div>
      </div>
    )
  }
}