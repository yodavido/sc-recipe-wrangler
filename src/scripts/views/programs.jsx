import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import ProgramList from '../components/programs/ProgramList.jsx';

@inject('programStore') @observer
export default class Programs extends Component {
  componentWillMount () {
    const { programStore } = this.props;
    programStore.getAllPrograms();
  }

  /**
   * Event handler for programs search
   * @param e
   */
  onSearchChange (e) {
    let { value } = e.target;
    this.props.programStore.searchProgram(value);
  }

  /**
   * Creates elements for css Loading animation
   * @returns {XML}
   * @constructor
   */
  Loader = () => {
    return(
      <div className="spinner">
        <div className="cube1"></div>
        <div className="cube2"></div>
      </div>
    )
  }

  render () {
    const { programStore } = this.props;
    const { programs, programsLoaded } = programStore;

    return (
      <div className="programs">
        <div className="hero">
          <div className="hero-body">
            <div className="container has-text-centered">
              <h1 className="title">{ programs ? programs.length : '' } Programs</h1>
              <h2 className="subtitle">Search Programs</h2>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column is-half is-offset-one-quarter search-box">
            <div className="field">
              <p className="control has-icons-left has-icons-right">
                <input className="input" type="text" placeholder="Search" onChange={(e)=>this.onSearchChange(e)}/>
                <span className="icon is-left">
                  <i className="fa fa-search"></i>
                </span>
              </p>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column is-12">
            { programsLoaded ?
                <ProgramList programs={programs}/> :
                this.Loader() }
          </div>
        </div>
      </div>
    )
  }
}