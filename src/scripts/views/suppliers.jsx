import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SupplierList   from '../components/suppliers/SupplierList.jsx';

@inject('supplierStore') @observer
export default class Suppliers extends Component {

  componentWillMount () {
    this.props.supplierStore.getAllSuppliers();
  }

  /**
   * Event handler for Supplier search.
   * @param e
   */
  onSearchChange (e) {
    let { value } = e.target;
    this.props.recipeStore.searchSuppliers(value);
  }

  /**
   * Creates elements for css Loading animation
   * @returns {XML}
   * @constructor
   */
  Loader = () => {
    return(
      <div className="spinner">
        <div className="cube1"></div>
        <div className="cube2"></div>
      </div>
    )
  }


  render () {
    const { supplierStore } = this.props;
    const { suppliers } = supplierStore;

    return (
      <div>
        <div className="hero">
          <div className="hero-body">
            <div className="container has-text-centered">
              <h1 className="title">{ suppliers ? suppliers.length : '' } Suppliers</h1>
              <h2 className="subtitle">Search suppliers</h2>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column is-half is-offset-one-quarter search-box">
            <div className="field">
              <p className="control has-icons-left has-icons-right">
                <input className="input" type="text" placeholder="Search" onChange={(e)=>this.onSearchChange(e)}/>
                <span className="icon is-left">
                  <i className="fa fa-search"></i>
                </span>
              </p>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column is-12">
            { suppliers.length > 0 ?
              <SupplierList suppliers={suppliers}/> :
              this.Loader() }
          </div>
        </div>
      </div>
    )
  }

}