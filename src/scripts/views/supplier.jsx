import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { observable } from 'mobx';

import ProgramList from '../components/programs/ProgramList.jsx';

@inject('programStore', 'supplierStore') @observer
export default class Supplier extends Component {
  @observable supplierName = "";

  componentDidMount() {
    const { supplierStore, programStore, params } = this.props;
    programStore.programsLoaded = false;
    supplierStore
      .getSupplierById(params._sid)
      .then(this.setProgramsFromSupplier.bind(this));

  }

  setProgramsFromSupplier (res) {
    const { programStore } = this.props;
    let convergeIds = [];

    this.supplierName = res.data[0].SupplierName;

    Object.keys(res.data[0].ConvergeIDs).forEach((key)=>{
      convergeIds.push({'ConvergeID': key, 'RecipeCount': res.data[0].ConvergeIDs[key]});
    });

    programStore.programs = convergeIds;
    programStore.programsLoaded = true;
  }

  onSearchChange (e) {
    let { value } = e.target;
    this.props.programStore.searchProgram(value);
  }

  render () {
    const { programStore, supplierStore } = this.props;
    const { programs, programsLoaded } = programStore;

    return (
      <div className="programs">
        <div className="hero">
          <div className="hero-body">
            <div className="container has-text-centered">
              <h1 className="title">{this.supplierName != "" ? <span>{this.supplierName} has: {programs.length}</span> : ""} Programs</h1>
              <h2 className="subtitle">Search Programs</h2>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column is-half is-offset-one-quarter search-box">
            <div className="field">
              <p className="control has-icons-left has-icons-right">
                <input className="input" type="text" placeholder="Search" onChange={(e)=>this.onSearchChange(e)}/>
                <span className="icon is-left">
                  <i className="fa fa-search"></i>
                </span>
              </p>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column is-12">
            { programsLoaded ?
              <ProgramList programs={programs}/> :
              <a className="button is-loading"></a> }
          </div>
        </div>
      </div>
    )
  }
}