import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import RecipesGrid   from '../components/recipes/RecipesGrid.jsx';
import RecipesList   from '../components/recipes/RecipesList.jsx';

import { RGrid } from '../states/Recipes';


let STATE = new RGrid();

@inject('recipeStore') @observer
export default class Program extends Component {

  componentWillMount () {
    const { params } = this.props;
    this.props.recipeStore.getRecipesByConverge(params._pid);
  }

  switchView (e) {
    e.preventDefault();
    STATE.viewMode = STATE.viewMode == "grid" ? "list" : "grid";
  }

  onSearchChange (e) {
    let { value } = e.target;
    this.props.recipeStore.searchRecipes(value);
  }

  Loader = () => {
    return(
      <div className="spinner">
        <div className="cube1"></div>
        <div className="cube2"></div>
      </div>
    )
  }

  render () {
    const { recipeStore, params } = this.props;
    const { recipes } = recipeStore;

    return (
      <div>
        <div className="hero">
          <div className="hero-body">
            <div className="container has-text-centered">
              <h1 className="title">{ recipeStore.recipesLoaded ? <span>{params._pid} has: {recipes.length}</span> : '' } Recipes</h1>
              <h2 className="subtitle">Search and update recipes</h2>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column is-half is-offset-one-quarter search-box">
            <div className="field">
              <p className="control has-icons-left has-icons-right">
                <input className="input" type="text" placeholder="Search" onChange={(e)=>this.onSearchChange(e)}/>
                <span className="icon is-left">
                  <i className="fa fa-search"></i>
                </span>
              </p>
            </div>
          </div>
          <div className="column is-one-quarter">
            <a href="" className="viewMode" onClick={this.switchView}>
              { STATE.viewMode == "grid" ?
                <i className="fa fa-th-list" aria-hidden="true"></i> :
                <i className="fa fa-th-large" aria-hidden="true"></i> }
            </a>
          </div>
        </div>
        <div className="columns">
          <div className="column is-12">
            { recipes.length > 0 ?
              (STATE.viewMode == "grid" ?
                <RecipesGrid recipes={recipes}/> :
                <RecipesList recipes={recipes}/>) :
              this.Loader() }
          </div>
        </div>
      </div>
    )
  }
}