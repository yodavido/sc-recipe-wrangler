import React, { Component } from 'react';
import { Link } from 'react-router';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import axios from 'axios';

@observer
export default class Home extends Component {
  @observable numbers = {};

  componentWillMount () {
    axios.get('/services/svcRecipeHubAdmin.asmx/GetNumbers_JSON')
      .then((res)=>{
        this.numbers = res.data;
      }).catch((error)=>console.log(error))
  }

  Loader = () => {
    return(
      <div className="spinner">
        <div className="cube1"></div>
        <div className="cube2"></div>
      </div>
    )
  }

  render () {
    return (
      <div className="home">
        <section className="hero is-large">
          <div className="hero-body">
            <div className="container">
              { this.numbers.SupplierCount ?
                <nav className="level">
                  <div className="level-item has-text-centered">
                    <Link className="home-link" to="/recipes">
                      <p className="heading">There are</p>
                      <p className="title"><span>{this.numbers.RecipeCount}</span> recipes</p>
                    </Link>
                  </div>
                  <div className="level-item has-text-centered">
                    <Link className="home-link" to="/suppliers">
                      <p className="heading">under</p>
                      <p className="title"><span>{this.numbers.SupplierCount}</span> suppliers</p>
                    </Link>
                  </div>
                  <div className="level-item has-text-centered">
                    <Link className="home-link" to="/programs">
                      <p className="heading">across</p>
                      <p className="title"><span>{this.numbers.ConvergeCount}</span> programs.</p>
                    </Link>
                  </div>
                </nav> :
                this.Loader()}
            </div>
          </div>
        </section>
      </div>
    )
  }
}