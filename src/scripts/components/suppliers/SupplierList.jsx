import React, { Component } from 'react';
import SupplierItem from './SupplierItem.jsx';

export default class SupplierList extends Component {
  render () {
    const { suppliers } = this.props;

    const SupplierItems = suppliers.map((s, i)=>{
      return <SupplierItem supplier={s} key={s.SupplierID} />;
    });

    return (
      <div className="columns supplier-items">
        {SupplierItems}
      </div>
    )
  }
}