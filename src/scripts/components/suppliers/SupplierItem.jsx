import React from 'react';
import { Link } from 'react-router';

const SupplierItem = props => {
  const { supplier } = props;

  return (
    <div className="column is-8 is-offset-2 supplier-item">
      <div className="box">
        <article className="media">
          <div className="media-content">
            <div className="content">
              <div className="level">
                <div className="level-left">
                  <p>
                    <strong dangerouslySetInnerHTML={{ __html: supplier.SupplierName}} />
                  </p>
                </div>
                <div className="level-right">
                  <Link to={`/suppliers/${supplier.SupplierID}`} className="card-footer-item">
                    <span className="icon is-small"><i className="fa fa-arrow-right"></i></span>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    </div>
  )
}

export default SupplierItem;