import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import { SingleDatePicker } from 'react-dates';
import moment from 'moment';

@observer
export default class DatePickr extends Component {
  @observable focused = false;

  render () {
    const { onDateChange, onCancel, defaultValue } = this.props;

    console.log(moment(defaultValue));

    return (
      <div className="field is-grouped date-picker">
        <div className="control">
          <SingleDatePicker
            date={defaultValue ? moment(defaultValue) : null}
            focused={this.focused}
            onFocusChange={({focused})=>this.focused=focused}
            onDateChange={onDateChange}
            numberOfMonths={1}
            placeholder='Select Live Date'
          />
        </div>
        <p className="control">
          <a className="card-footer-item"
             style={{
               borderRight: '1px solid #dbdbdb'
             }}
             onClick={onCancel}>
              <span className="icon is-small"><i className="fa fa-close"></i></span>
          </a>
        </p>
      </div>
    )
  }

}