import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable } from 'mobx';

@observer
export default class EditableLine extends Component {
  @observable editing = false;
  @observable val = "";

  onUpdateLine () {
    const { lineName, recipe } = this.props;
    recipe.setProp(lineName, ( this.val != "" && this.val != " " ) ? this.val : recipe[lineName]);
    this.editing = false;
    recipe.updateRecipe();
  }

  render () {
    const { lineName, lineLabel, recipe, isNumber, isTextArea} = this.props;

    return (
      <div>
        <a style={{position: 'absolute', right: '-20px', top: '-10px'}}><span className="icon is-small" onClick={()=>this.editing = true}><i className="fa fa-pencil"></i></span></a>
        <div className={`modal ${this.editing ? 'is-active' : ''}`}>
          <div className="modal-background">
            <div className="modal-content" style={{top: '45%'}}>
              <div className="card">
                <header className="card-header">
                  <p className="card-header-title subtitle">Current {lineLabel}: {recipe[lineName]}</p>
                </header>
                <div className="card-content">
                  <div className="field is-grouped">
                    <p className="control is-expanded">
                      { isTextArea ?
                        <textarea className="textarea" placeholder={recipe[lineName]} onChange={(e)=>this.val=e.target.value} ></textarea> :
                        <input className="input" type={isNumber ? "number" : "text"} placeholder={recipe[lineName]} onChange={(e)=>this.val=e.target.value} />
                      }
                    </p>
                    <p className="control">
                      <a onClick={this.onUpdateLine.bind(this)}><span className="icon is-small"><i className="fa fa-check"></i></span></a>
                      <a onClick={()=>this.editing=false}><span className="icon is-small"><i className="fa fa-close"></i></span></a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }
}