import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable } from 'mobx';

@observer
export default class RecipeMediaEditor extends Component {
  @observable editing = false;
  @observable val = '';
  @observable mediaType = '';
  @observable previewing = false;

  componentDidMount () {
    this.mediaType = this.props.recipe.MediaType ? this.props.recipe.MediaType : 'image';
  }

  onUpdateLine () {
    const { recipe } = this.props;
    recipe.setProp('RecipeImage', ( this.val != "" && this.val != " " ) ? this.val : recipe['RecipeImage']);
    recipe.setProp('MediaType', this.mediaType);
    this.editing = false;
    recipe.updateRecipe();
  }

  renderMediaPreview () {
    const { recipe } = this.props;

    return (
      <div className='modal is-active'>
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head" style={{justifyContent: 'flex-end'}}>
            <button className="delete" onClick={()=>this.previewing = false}></button>
          </header>
          <section className="modal-card-body">
            { (recipe.MediaType == "image" || recipe.MediaType == "") ?
              <img src={`/Featured-Brand/_RecipeImages/${recipe.ConvergeIDs[0]}/${recipe.RecipeImage}.jpg`} alt="Image" /> :
              <video
                width="640px"
                height="320px"
                controls
              >
                <source src={`https://static-clients.secure.footprint.net/SamsClub/_RecipeVideos/${recipe.ConvergeIDs[0]}/${recipe.RecipeImage}.mp4`} type="video/mp4" />
              </video> }
          </section>
        </div>
      </div>
    )
  }

  render () {
    const { recipe } = this.props;

    return (
      <div>
        {this.previewing ? this.renderMediaPreview(recipe.MediaType) : ''}

        <a onClick={()=>this.previewing = true}>
          <figure className="image is-128x128">
            { recipe.ConvergeIDs ?
              <img src={`/Featured-Brand/_RecipeImages/${recipe.ConvergeIDs[0]}/${recipe.RecipeImage}.jpg`} alt="Image" /> :
              <img src="http://bulma.io/images/placeholders/480x480.png" alt=""/>
            }
          </figure>
          { recipe.MediaType == "video" ?
            <figure className="image is-128x128" style={{position: 'absolute', left: '50%', marginLeft: '-64px', top: '50%', marginTop:'-64px', zIndex: 19}}>
              <i className="fa fa-play-circle-o" aria-hidden="true" style={{color: '#fff', fontSize: '8.5em'}}></i>
            </figure> :
            ''}
        </a>
        <a style={{position: 'absolute', right: '-20px', top: '-10px'}}><span className="icon is-small" onClick={()=>this.editing = true}><i className="fa fa-pencil"></i></span></a>
        <div className={`modal ${this.editing ? 'is-active' : ''}`}>
          <div className="modal-background">
            <div className="modal-content" style={{top: '45%'}}>
              <div className="card">
                <header className="card-header">
                  <span className="card-header-title subtitle">Current Media Name: {recipe.RecipeImage}</span>
                </header>
                <div className="card-content">
                  <div className="field is-horizontal">
                    <div className="field-label">
                      <label className="label">Media Type: </label>
                    </div>
                    <div className="field-body">
                      <div className="field is-narrow">
                        <div className="control">
                          <label className="radio">
                            <input type="radio" name="member" checked={this.mediaType == "image"} onClick={()=>this.mediaType = 'image'}/>
                              Image
                          </label>
                          <label className="radio">
                            <input type="radio" name="member" checked={this.mediaType == "video"} onClick={()=>this.mediaType = 'video'}/>
                              Video
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>


                  <div className="field is-grouped">
                    <span className="control is-expanded">
                      <input className="input" type="text" placeholder={recipe.RecipeImage} onChange={(e)=>this.val=e.target.value} />
                    </span>
                    <span className="control">
                      <a onClick={this.onUpdateLine.bind(this)}><span className="icon is-small"><i className="fa fa-check"></i></span></a>
                      <a onClick={()=>this.editing=false}><span className="icon is-small"><i className="fa fa-close"></i></span></a>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }

}