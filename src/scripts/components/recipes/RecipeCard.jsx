import React from 'react';
import { Link } from 'react-router';

const RecipeCard = props => {
  const { recipe } = props;

  return (
    <div className="column is-one-third recipe-card">
      <div className="card">
        <header className="card-header">
          <p className="card-header-title" dangerouslySetInnerHTML={{ __html: recipe.RecipeName }} />
        </header>
        <div className="card-image">
          <figure className="image is-4by3">
            <img src={`/Featured-Brand/_RecipeImages/${recipe.ConvergeIDs[0]}/${recipe.RecipeImage}.jpg`} alt="Image" />
          </figure>
          { recipe.MediaType == "video" ?
            <figure className="image is-128x128" style={{position: 'absolute', left: '50%', marginLeft: '-64px', top: '50%', marginTop:'-64px', zIndex: 100}}>
              <i className="fa fa-play-circle-o" aria-hidden="true" style={{color: '#fff', fontSize: '8.5em'}}></i>
            </figure> :
            ''}
        </div>
        <footer className="card-footer">
          <Link to={`/recipes/${recipe.RecipeID}`} className="card-footer-item"><span className="icon is-small"><i className="fa fa-pencil"></i></span> View/Edit</Link>
        </footer>
      </div>
    </div>
  )
}

export default RecipeCard;