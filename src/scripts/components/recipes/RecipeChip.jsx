import React from 'react';

export const RecipeChip = props => {
  const { val } = props;

  return <span className="tag is-info">{ val }</span>;
}

export const RecipeChipDeletable = props => {
  const { val, deleteChip } = props;

  return <span className="tag is-info">{ val } <button className="delete is-small" onClick={deleteChip}></button></span>;
}

export const RecipeChipEditable = props => {
  return (
    <span className="tag is-info">
      <div className="field">
        <p className="control">
          <input id="listItemEdit" className="input" type="text" placeholder={val} defaultValue={val}/>
        </p>
      </div>
    </span>
  )
}