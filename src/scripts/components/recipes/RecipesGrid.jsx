import React, { Component } from 'react';
import RecipeCard from './RecipeCard.jsx';

export default class RecipesGrid extends Component {

  render () {
    const { recipes } = this.props;

    const RecipeCards = recipes.map((r, i)=>{
      return <RecipeCard recipe={r} key={r.RecipeID} />;
    });

    return (
      <div className="columns">
        {RecipeCards}
      </div>
    )
  }

}