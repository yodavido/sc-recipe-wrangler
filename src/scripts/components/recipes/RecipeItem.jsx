import React from 'react';
import { Link } from 'react-router';

const RecipeItem = props => {
  const { recipe } = props;

  return (
    <div className="column is-8 is-offset-2 recipe-item">
      <div className="box">
        <article className="media">
          <div className="media-left">
            <figure className="image is-64x64">
              <img src={`/Featured-Brand/_RecipeImages/${recipe.ConvergeIDs[0]}/${recipe.RecipeImage}.jpg`} alt="Image" />
            </figure>
          </div>
          <div className="media-content">
            <div className="content">
              <div className="level">
                <div className="level-left">
                  <p>
                    <strong dangerouslySetInnerHTML={{ __html: recipe.RecipeName}} />
                  </p>
                </div>
                <div className="level-right">
                  <Link to={`/recipes/${recipe.RecipeID}`} className="card-footer-item"><span className="icon is-small"><i className="fa fa-pencil"></i></span></Link>
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    </div>
  )
}

export default RecipeItem;