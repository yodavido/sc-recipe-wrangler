import React, { Component } from 'react';
import { observable } from 'mobx';
import { observer } from 'mobx-react';

const ListLink = props => {
  const { val } = props;
  let linkParts = val.split("##");
  let track = "";

  if(linkParts.length > 2) {
    track = linkParts[1].replace(" ", "_");
    if (linkParts.length > 3)
      return (
        <div>
          {linkParts[0]}
          <a
            is
            href={`${linkParts[2]}`}
            track={track}
            dangerouslySetInnerHTML={{ __html: linkParts[1]}} />
          {linkParts[3]}
        </div>
      )
    else
      return (
        <div>
          {linkParts[0]}
          <a
            href={`${linkParts[2]}`}
            track={track}
            dangerouslySetInnerHTML={{ __html: linkParts[1] }} />
        </div>
      )
  } else {
    track = linkParts[0].replace(" ", "_");
    return <a href={`${linkParts[1]}`} track={tracker} dangerouslySetInnerHTML={{ __html: linkParts[1] }} />
  }
}

const ListItemEditor = props => {
  const { val } = props;

  return (
    <div className="field">
      <p className="control">
        <input id="listItemEdit" className="input" type="text" placeholder={val} defaultValue={val}/>
      </p>
    </div>
  )
}

const ListItemActions = props => {
  const { editing, onEdit, onCancel, onSave, onDelete } = props;

  return (
    <div className="column is-1">
      { editing ?
        <span className="editing-item">
          <a onClick={(e)=>onSave(e)}><span className="icon is-small"><i className="fa fa-check"></i></span></a>
          <a onClick={(e=>onCancel(e))}><span className="icon is-small"><i className="fa fa-close"></i></span></a>
        </span>:
        <span>
          <a href="" onClick={ (e)=>onEdit(e) }>
            <span className="icon is-small"><i className="fa fa-pencil"></i></span>
          </a>
          <a href="" onClick={(e)=>onDelete(e)}><span className="icon is-small"><i className="fa fa-trash"></i></span></a>
        </span>
      }
    </div>
  )
}

@observer
export default class RecipeListItem extends Component {
  @observable editing = false;

  onEdit (e) {
    e.preventDefault();
    this.editing = true;
  }

  onDelete (e) {
    e.preventDefault();
    let { onListItemDelete, index } = this.props;
    onListItemDelete(index);
  }

  onSave (e) {
    e.preventDefault();
    let { onListItemSave, index } = this.props;
    const elem = document.getElementById('listItemEdit');
    const elemVal = elem.value;

    onListItemSave(index, elemVal);

    this.editing = false;
  }

  onCancel (e) {
    e.preventDefault();
    this.editing = false;
  }

  render() {
    const { val, onListItemChange, index } = this.props;

    return (
      <div className="panel-block" ref={node => {this.node = node}}
           draggable={this.editing ? "false" : "true"}
           data-id={index}
           onDragStart={()=>console.log('dragging')}
           onDragEnd={()=>console.log('done dragging')}>
        <div className="columns is-fullwidth">
          <div className="column is-11">
            { this.editing ?
              <ListItemEditor val={val} onChange={ onListItemChange }/> :
              <div>
                {val.search('##') > -1 ? <ListLink val={val}/> : <span dangerouslySetInnerHTML={{ __html: val }} />}
              </div>
            }
          </div>

          <ListItemActions editing={this.editing} onEdit={this.onEdit.bind(this)} onSave={this.onSave.bind(this)} onCancel={this.onCancel.bind(this)} onDelete={this.onDelete.bind(this)} />
        </div>
      </div>
    )
  }
}