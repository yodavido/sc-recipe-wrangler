import React, { Component } from 'react';
import RecipeItem from './RecipeItem.jsx';

export default class RecipesList extends Component {
  render () {
    const { recipes } = this.props;

    const RecipeItems = recipes.map((r, i)=>{
      return <RecipeItem recipe={r} key={r.RecipeID} />;
    });

    return (
      <div className="columns recipe-items">
        {RecipeItems}
      </div>
    )
  }
}