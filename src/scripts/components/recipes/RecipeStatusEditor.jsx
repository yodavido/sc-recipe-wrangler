import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable } from 'mobx';

import Switch from 'react-toggle-switch';

@observer
export default class RecipeStatusEditor extends Component {
  @observable editing = false;

  onUpdateStatus () {
    const { recipe } = this.props;
    recipe.updateRecipe();
    this.editing=false;
  }

  render () {
    const { recipe } = this.props;

    return (
      <span>
        <a style={{position: 'absolute', right: '-20px', top: '-10px'}}><span className="icon is-small" onClick={()=>this.editing = true}><i className="fa fa-pencil"></i></span></a>

        { recipe.Approved ?
          <span className="icon"><i className="fa fa-circle"></i></span> :
          <span className="icon"><i className="fa fa-circle is-warning"></i></span>
        }
        { recipe.Featured ?
          <span><span className="icon"><i className="fa fa-star"></i></span> Featured</span> :
          <span><span className="icon"><i className="fa fa-star-o"></i></span> Featured</span>
        }
        <div className={`modal ${this.editing ? 'is-active' : ''}`}>
          <div className="modal-background">
            <div className="modal-content" style={{top: '45%'}}>
              <div className="card">
                <header className="card-header">
                  <p className="card-header-title subtitle">
                    <Switch on={recipe.Approved} onClick={()=>recipe.setProp('Approved', !recipe.Approved)}/> &nbsp; Approved
                  </p>
                </header>
                <header className="card-header">
                  <p>
                    Approved On will show recipe in any program its in. Approved Off will prevent it from being shown globally.
                  </p>
                </header>
                <header className="card-header">
                  <p className="card-header-title subtitle">
                    <Switch on={recipe.Featured} onClick={()=>recipe.setProp('Featured', !recipe.Featured)}/> &nbsp; Featured
                  </p>
                </header>
                <div className="card-content">
                  <div className="field is-grouped">
                    <p className="control is-expanded">
                    </p>
                    <p className="control">
                      <a onClick={this.onUpdateStatus.bind(this)}><span className="icon is-small"><i className="fa fa-check"></i></span></a>
                      <a onClick={()=>this.editing=false}><span className="icon is-small"><i className="fa fa-close"></i></span></a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </span>
    )
  }
}