import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import { Link } from 'react-router';

import DatePickr from '../global/DatePickr.jsx';
import EditableLine from '../global/EditableLine.jsx';
import RecipeListItem from '../recipes/RecipeListItem.jsx';
import RecipeStatusEditor from '../recipes/RecipeStatusEditor.jsx';
import RecipeMediaEditor from '../recipes/RecipeMediaEditor.jsx';
import { RecipeChipDeletable } from '../recipes/RecipeChip.jsx';

@observer
export default class RecipeNewStep3 extends Component {
  @observable isAddingIng = false;
  @observable isAddingIns = false;
  @observable isAddingCat = false;
  @observable isAddingCourse = false;
  @observable isAddingDate = false;

  componentWillMount () {
    this.props.recipeStore.initNewRecipe();
    this.props.recipeStore.recipe.setProp('ConvergeIDs', [this.props.converge]);
  }

  /**
   * Handles live date change.
   */
  liveDateChange (e) {
    const { recipe } = this.props.recipeStore;
    recipe.LiveDate = new Date(e._d);
    recipe.updateRecipe();
    this.isAddingDate = false;
  }
  cancelDateChange () {
    this.isAddingDate = false;
  }
  /**
   * Handles opening a new ingredient input, saving new ingredient,
   * saving updated ingredients and deleting ingredient.
   * @param e
   */
  addingIngredient (e) {
    e.preventDefault();
    this.isAddingIng = true;
  }
  addIngredient (e) {
    e.preventDefault();
    let { recipe } = this.props.recipeStore;
    let elem = document.getElementById('ingItemAdd');
    let val = elem.value;

    recipe.addIngredient(val);
    recipe.updateRecipe();
    this.isAddingIng = false;
  }
  updateIngredient (index, val) {
    let { recipe } = this.props.recipeStore;
    recipe.updateIngredient(index, val);
    recipe.updateRecipe();
  }
  deleteIngredient (index) {
    let { recipe } = this.props.recipeStore;
    recipe.deleteIngredient(index);
    recipe.updateRecipe();
  }

  /**
   * Handles opening a new instruction input, saving new instruction,
   * saving updated instruction and deleting instruction.
   * @param e
   */
  addingInstruction (e) {
    e.preventDefault();
    this.isAddingIns = true;
  }
  addInstruction (e) {
    e.preventDefault();
    let { recipe } = this.props.recipeStore;
    let elem = document.getElementById('insItemAdd');
    let val = elem.value;

    recipe.addInstruction(val);
    recipe.updateRecipe();
    this.isAddingIns = false;
  }
  updateInstruction (index, val) {
    let { recipe } = this.props.recipeStore;
    recipe.updateInstruction(index, val);
    recipe.updateRecipe();
  }
  deleteInstruction (index) {
    let { recipe } = this.props.recipeStore;
    recipe.deleteInstruction(index);
    recipe.updateRecipe();
  }

  /**
   * Handles opening a new course input, saving new course
   * and deleting course.
   * @param e
   */
  addingCourse (e) {
    e.preventDefault();
    this.isAddingCourse = true;
  }
  addCourse (e) {
    e.preventDefault();
    let { recipe } = this.props.recipeStore;
    let elem = document.getElementById('courseItemAdd');
    let val = elem.value;

    recipe.addCourse(val);
    recipe.updateRecipe();
    this.isAddingCourse = false;
  }
  deleteCourse (c) {
    let { recipe } = this.props.recipeStore;
    recipe.removeCourse(c);
    recipe.updateRecipe();
  }

  /**
   * Handles opening a new category input, saving new category
   * and deleting category.
   * @param e
   */
  addingCat (e) {
    e.preventDefault();
    this.isAddingCat = true;
  }
  addCat (e) {
    e.preventDefault();
    let { recipe } = this.props.recipeStore;
    let elem = document.getElementById('catItemAdd');
    let val = elem.value;

    recipe.addCategory(val);
    recipe.updateRecipe();
    this.isAddingCat = false;
  }
  deleteCat (c) {
    let { recipe } = this.props.recipeStore;
    recipe.removeCategory(c);
    recipe.updateRecipe();
  }

  /**
   * cancels all new inputs.
   * @param e
   */
  cancelAdding (e) {
    e.preventDefault();
    this.isAddingIng = false;
    this.isAddingIns = false;
    this.isAddingCourse = false;
    this.isAddingCat = false;
  }

  onSaveRecipe (e) {
    const { recipeStore } = this.props;
    const { recipe } = recipeStore;
    recipeStore.addNewRecipe(recipe);
  }

  render () {
    const { recipeStore, supplierStore } = this.props;
    const { recipe } = recipeStore;

    return (
      <div className="recipe">
        <div className="hero">
          <div className="hero-body">
            <div className="container has-text-centered">
              <div className="columns">
                <div className="column is-half is-offset-one-quarter">
                  <div className="level">
                    <div className="level-left is-small">
                      <span className="make-it-inline">
                        <RecipeStatusEditor recipe={recipe} />
                      </span>
                    </div>
                    <div className="level-right is-small">
                      {this.isAddingDate ?
                        <DatePickr onDateChange={this.liveDateChange.bind(this)} onCancel={this.cancelDateChange.bind(this)} defaultValue={recipe.LiveDate}/>:
                        <a className="card-footer-item"
                           onClick={()=>this.isAddingDate = true}>
                          <span className="icon is-small"><i className="fa fa-calendar"></i></span>
                        </a>}
                      <a className="card-footer-item" onClick={this.onSaveRecipe.bind(this)}><span className="icon is-small"><i className="fa fa-save"></i></span></a>
                      <Link to="/" className="card-footer-item"><span className="icon is-small"><i className="fa fa-close"></i></span></Link>
                    </div>
                  </div>
                </div>
              </div>
              <h1 className="title">
                <strong className="make-it-inline">{recipe.RecipeName}<EditableLine lineName="RecipeName" lineLabel="Recipe Name" recipe={ recipe } isNumber={false} isTextArea={false} /></strong>
              </h1>
            </div>
          </div>
        </div>
        <div>
          <nav className="level recipe-times">
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Prep Time</p>
                <span className="title make-it-inline">
                  {recipe.PrepTime} min.<EditableLine lineName="PrepTime" lineLabel="Prep Time in min" recipe={ recipe } isNumber={true} isTextArea={false} />
                </span>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Cook Time</p>
                <span className="title make-it-inline">
                  { recipe.CookTime } min.<EditableLine lineName="CookTime" lineLabel="Cook Time in min" recipe={ recipe } isNumber={true} isTextArea={false} />
                </span>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Servings</p>
                <span className="title make-it-inline">
                  { recipe.Servings }<EditableLine lineName="Servings" lineLabel="Servings" recipe={ recipe } isNumber={true} isTextArea={false} />
                </span>
              </div>
            </div>
          </nav>
          <span className="make-it-block" style={{maxWidth: '480px'
          }}>
            <RecipeMediaEditor recipe={recipe} />
          </span>

        </div>
        <div className="columns">
          <div className="column is-5 is-centered">
            <span className="make-it-inline">
              {recipe.RomanceCopy ? recipe.RomanceCopy : "Add Some Romance Copy."}
              <EditableLine lineName="RomanceCopy" lineLabel="Romance Copy" recipe={ recipe } isNumber={false} isTextArea={true} />
            </span>
          </div>
        </div>
        <div className="columns">
          <div className="column is-5 is-centered">
            <span className="is-centered make-it-inline">
              <strong>Recipe Notes:</strong> {recipe.RecipeNotes ? recipe.RecipeNotes : "Add Some Notes."}
              <EditableLine lineName="RecipeNotes" lineLabel="Notes" recipe={ recipe } isNumber={false} isTextArea={true} />
            </span>
          </div>
        </div>
        <div className="columns">
          <div className="column is-10 is-offset-1">
            <div className="box">
              <h2 className="subtitle has-text-centered">Ingredients</h2>
            </div>
            <div className="panel">
              { recipe.Ingredients ? recipe.Ingredients.map((c, i) => {
                return ( <RecipeListItem val={c.toString().replace(/\\n/g, '')}
                                         key={i} index={i}
                                         onListItemSave={this.updateIngredient.bind(this)}
                                         onListItemDelete={this.deleteIngredient.bind(this)} /> )}) : ''}
              { this.isAddingIng ?
                <div className="panel-block">
                  <div className="columns is-fullwidth">
                    <div className="column is-11">
                      <div className="field">
                        <p className="control">
                          <input id="ingItemAdd" className="input" type="text" placeholder="Ingredient Format: Ingredient ##LINK TEXT##LINK URL## More Ingredient"/>
                        </p>
                      </div>
                    </div>
                    <div className="column is-1">
                      <span className="editing-item">
                        <a href="" onClick={this.addIngredient.bind(this)}><span className="icon is-small"><i className="fa fa-check"></i></span></a>
                        <a href="" onClick={this.cancelAdding.bind(this)}><span className="icon is-small"><i className="fa fa-close"></i></span></a>
                      </span>
                    </div>
                  </div>
                </div>:
                <div className="columns is-fullwidth">
                  <div className="column is-2">
                    <a href="" className="button is-success is-outlined is-fullwidth" onClick={this.addingIngredient.bind(this)}>+</a>
                  </div>
                </div> }
            </div>
            <div className="box">
              <h2 className="subtitle has-text-centered">Directions</h2>
            </div>
            <div className="panel">
              { recipe.CookingInstructions ? recipe.CookingInstructions.map((c, i) => {
                return ( <RecipeListItem val={c.toString().replace(/\\n/g, '')}
                                         key={i} index={i}
                                         onListItemSave={this.updateInstruction.bind(this)}
                                         onListItemDelete={this.deleteInstruction.bind(this)} />)}) : ''}
              { this.isAddingIns ?
                <div className="panel-block">
                  <div className="columns is-fullwidth">
                    <div className="column is-11">
                      <div className="field">
                        <p className="control">
                          <input id="insItemAdd" className="input" type="text" placeholder="Direction Format: Direction ##LINK TEXT##LINK URL## More Direction"/>
                        </p>
                      </div>
                    </div>
                    <div className="column is-1">
                      <span className="editing-item">
                        <a href="" onClick={this.addInstruction.bind(this)}><span className="icon is-small"><i className="fa fa-check"></i></span></a>
                        <a href="" onClick={this.cancelAdding.bind(this)}><span className="icon is-small"><i className="fa fa-close"></i></span></a>
                      </span>
                    </div>
                  </div>
                </div> :
                <div className="columns is-fullwidth">
                  <div className="column is-2">
                    <a href="" className="button is-success is-outlined is-fullwidth" onClick={this.addingInstruction.bind(this)}>+</a>
                  </div>
                </div> }
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column is-one-quarter is-offset-one-quarter">
            <h2 className="subtitle has-text-centered">Courses</h2>
            <div className="courses">
              { recipe.Course && recipe.Course.length > 0 && recipe.Course[0] !== "" ?
                recipe.Course.map((c, i)=>{return <RecipeChipDeletable val={c} key={c} deleteChip={()=>this.deleteCourse(c)}/>;}) :
                <h2 className="subtitle has-text-centered">Add a Course.</h2>}
            </div>
            <div className="columns is-fullwidth chip-add">
              <div className="column is-4">
                { this.isAddingCourse ?
                  <div className="field is-grouped">
                    <p className="control">
                      <input id="courseItemAdd" className="input" type="text" placeholder="Course" />
                    </p>
                    <p className="control">
                      <a href="" onClick={this.addCourse.bind(this)}><span className="icon is-small"><i className="fa fa-check"></i></span></a>
                      <a href="" onClick={this.cancelAdding.bind(this)}><span className="icon is-small"><i className="fa fa-close"></i></span></a>
                    </p>
                  </div> :
                  <a href="" className="button is-success is-outlined is-fullwidth is-small" onClick={this.addingCourse.bind(this)}>+</a> }
              </div>
            </div>
          </div>
          <div className="column is-one-quarter">
            <h2 className="subtitle has-text-centered">Categories</h2>
            <div className="categories">
              { recipe.Category && recipe.Category.length > 0 && recipe.Category[0] !== "" ?
                recipe.Category.map((c, i)=>{return <RecipeChipDeletable val={c} key={c} deleteChip={()=>this.deleteCat(c)} />;}) :
                <h2 className="subtitle has-text-centered">Add a Category.</h2>}
            </div>
            <div className="columns is-fullwidth chip-add">
              <div className="column is-4">
                { this.isAddingCat ?
                  <div className="field is-grouped">
                    <p className="control">
                      <input id="catItemAdd" className="input" type="text" placeholder="Category" />
                    </p>
                    <p className="control">
                      <a href="" onClick={this.addCat.bind(this)}><span className="icon is-small"><i className="fa fa-check"></i></span></a>
                      <a href="" onClick={this.cancelAdding.bind(this)}><span className="icon is-small"><i className="fa fa-close"></i></span></a>
                    </p>
                  </div> :
                  <a href="" className="button is-success is-outlined is-fullwidth is-small" onClick={this.addingCat.bind(this)}>+</a> }
              </div>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column is-5 is-centered">
            <div className="box">
              <h2 className="subtitle has-text-centered">Notes & Tracking</h2>
            </div>
            <div className="panel">
              <div className="panel-block">
                <span className="is-centered make-it-inline"><strong>Tracking Name:</strong> {recipe.Tracking ? recipe.Tracking : "Add Tracking Name."}<EditableLine lineName="Tracking" lineLabel="Tracking" recipe={ recipe } isNumber={false} isTextArea={false} /></span>
              </div>
              <div className="panel-block">
                <span className="is-centered make-it-inline"><strong>Brand Name:</strong> {recipe.Brand ? recipe.Brand : "Add Brand Name."}<EditableLine lineName="Brand" lineLabel="Brand" recipe={ recipe } isNumber={false} isTextArea={false} /></span>
              </div>
              <div className="panel-block">
                <span className="is-centered make-it-inline"><strong>Logo Name:</strong> {recipe.LogoImage ? recipe.LogoImage : "Add Logo."}<EditableLine lineName="LogoImage" lineLabel="Logo Name" recipe={ recipe } isNumber={false} isTextArea={false} /></span>
              </div>
              <div className="panel-block">
                <strong>Converge ID:</strong> {recipe.ConvergeIDs[0]}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

RecipeNewStep3.propTypes = {
  recipeStore: PropTypes.object,
  converge: PropTypes.string
}