import React, { Component } from 'react';
import ProgramItem from './ProgramItem.jsx';

export default class ProgramList extends Component {
  render () {
    const { programs } = this.props;

    const ProgramItems = programs.map((p, i)=>{
      return <ProgramItem program={p} key={p.ConvergeID} />;
    });

    return (
      <div className="columns supplier-items">
        {ProgramItems}
      </div>
    )
  }
}