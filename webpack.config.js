var path = require("path");
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  context: path.join(__dirname, 'src'),
  devServer: {
    open: true,
    inline: true,
    contentBase: path.join(__dirname + '/src')
  },
  entry: {
    app: ["./scripts/main.jsx"]
  },
  output: {
    path: path.join(__dirname, "src"),
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.jsx$/,
        loaders: ['babel-loader'],
        exclude: /(node_modules)/
      },
      {
        test: /\.js$/,
        loaders: ['babel-loader'],
        exclude: /(node_modules)/
      },
      { // regular css files
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          use: 'style-loader!css-loader?importLoaders=1',
        }),
      },
      { // sass / scss loader for webpack
        test: /\.(sass|scss)$/,
        loader: 'style-loader!css-loader!sass-loader'
      }
    ]
  }
}